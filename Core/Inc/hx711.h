#ifndef APPLICATION_CORE_HX711_H_
#define APPLICATION_CORE_HX711_H_

#include "stdint.h"
#include "stdbool.h"
#include "main.h"

// If these macros are in use, this will be guarantee the result will be in grams.
// This accuracy of the result will be enough in this job.
#define ONE_TIME 1
#define TEN_TIMES 10
#define HUNDRED_TIMES 100
#define THOUSAND_TIMES 1000


#define interrupts() __enable_irq()
#define noInterrupts() __disable_irq()

typedef struct
{
  GPIO_TypeDef *sck_gpio;
  GPIO_TypeDef  *dt_gpio;
  uint16_t      sck_pin;
  uint16_t      dt_pin;
  int32_t       offset;
  float         scale;
  uint8_t		gain;
}hx711_t;

/*
 * Setup functions
 */
void hx711_init(hx711_t *hx711, GPIO_TypeDef *sck_gpio, uint16_t sck_pin, GPIO_TypeDef *dt_gpio, uint16_t dt_pin);
void set_scale(hx711_t *hx711, float scale);
void set_gain(hx711_t *hx711, uint8_t gain);
void set_offset(hx711_t *hx711, int32_t offset);

/*
 * Load cell reading functions
 */
void tare(hx711_t *hx711, uint8_t times);
int32_t get_weight(hx711_t *hx711, int8_t times);

/*
 * Utility functions
 */

bool is_ready(hx711_t *hx711);
void wait_ready(hx711_t *hx711);
int32_t read_from_hx711(hx711_t *hx711);
int32_t read_average(hx711_t *hx711, int8_t times);
int32_t get_value(hx711_t *hx711, int8_t times);


#endif /* APPLICATION_CORE_HX711_H_ */
