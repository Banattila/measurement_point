/*
 * battery.h
 *
 *  Created on: Dec 3, 2023
 *      Author: banpa
 */

#ifndef INC_BATTERY_H_
#define INC_BATTERY_H_

#include <adc.h>
#include <main.h>

#define MAX_LSB 4095
#define MIN_VOLTAGE 3.4
#define MAX_VOLTAGE 4.2

uint16_t measure_battery(ADC_HandleTypeDef* adc);
uint8_t battery_in_percent(ADC_HandleTypeDef* adc);

#endif /* INC_BATTERY_H_ */
