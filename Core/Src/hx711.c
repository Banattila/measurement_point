#include <HX711.h>


// This function setting up the GPIO pins.
void hx711_init(hx711_t *hx711, GPIO_TypeDef *sck_gpio, uint16_t sck_pin, GPIO_TypeDef *dt_gpio, uint16_t dt_pin){
  hx711->sck_gpio = sck_gpio;
  hx711->sck_pin = sck_pin;
  hx711->dt_gpio = dt_gpio;
  hx711->dt_pin = dt_pin;
}

// The used scale is setting.
void set_scale(hx711_t *hx711, float scale){
	hx711->scale = scale;
}

// The used gain is setting. It need to the reduce the linearity mistake.
void set_gain(hx711_t *hx711, uint8_t gain){
	switch (gain) {
			case 128:
				hx711->gain = 1;
				break;
			case 64:
				hx711->gain = 3;
				break;
		}
}

// The used offset is setting. It need to reduce the zero mistake.
void set_offset(hx711_t *hx711, int32_t offset){
	hx711->offset = offset;
}

// If the module ready for measurement when the data pin will be low level. This function is checking it.
bool is_ready(hx711_t *hx711) {
	if(HAL_GPIO_ReadPin(hx711->dt_gpio, hx711->dt_pin) == GPIO_PIN_RESET){
		return 1;
	}
	return 0;
}

// Waiting while the module will be ready.
void wait_ready(hx711_t *hx711) {
	while (!is_ready(hx711)) {
		HAL_Delay(0);
	}
}

// Reading data from the module.
int32_t read_from_hx711(hx711_t *hx711) {
    wait_ready(hx711);
    int32_t value = 0;
    uint8_t sign = 0x00;

    // All interrupts are disabling.
    noInterrupts();


    // It is reading the 24 bits from module.
    for (int8_t i = 0; i < 24; ++i) {
        HAL_GPIO_WritePin(hx711->sck_gpio, hx711->sck_pin, SET);
        value = (value << 1) | HAL_GPIO_ReadPin(hx711->dt_gpio, hx711->dt_pin);
        HAL_GPIO_WritePin(hx711->sck_gpio, hx711->sck_pin, RESET);
    }

    // It setting up the module gain.
    for (int8_t i = 0; i < hx711->gain; i++) {
        HAL_GPIO_WritePin(hx711->sck_gpio, hx711->sck_pin, SET);
        HAL_GPIO_WritePin(hx711->sck_gpio, hx711->sck_pin, RESET);
    }

    // Interrupts are enabling.
    interrupts();

    // If the number is negative, we add negative sign bits to the most significant bits.
    if (value & 0x800000) {
        sign = 0xFF;
    }

    // It is adding the sign to the number.
    value = ( (uint32_t)(sign) << 24 | value );

    // Return with signed value.
    return value;
}

// Reading from module one or more than one time. This is going to make an average. In header file has some macro for the time parameter.
// If these macros are in use,  it is guarantee than the result will be in grams.
int32_t read_average(hx711_t *hx711, int8_t times) {
	int32_t sum = 0;
	for (int8_t i = 0; i < times; i++) {
		sum += read_from_hx711(hx711);
		HAL_Delay(0);
	}
	return sum / times;
}

// The value from the module will be corrected with the offset.
int32_t get_value(hx711_t *hx711, int8_t times) {
	return read_average(hx711, times) - hx711->offset;
}


// This is a utility function for tare of the scale setting up.
void tare(hx711_t *hx711, uint8_t times) {
	read_from_hx711(hx711);
	double sum = read_average(hx711, times);
	set_offset(hx711, sum);
}

// The result from the module will be corrected with offset and the scale. This will be the final result.
int32_t get_weight(hx711_t *hx711, int8_t times) {
	int32_t result = get_value(hx711, times);
	result /= hx711->scale;
	return result;
}
