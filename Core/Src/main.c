/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file           : main.c
 * @brief          : Main program body
 ******************************************************************************
 * @attention
 *
 * Copyright (c) 2023 STMicroelectronics.
 * All rights reserved.
 *
 * This software is licensed under terms that can be found in the LICENSE file
 * in the root directory of this software component.
 * If no LICENSE file comes with this software, it is provided AS-IS.
 *
 ******************************************************************************
 */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "adc.h"
#include "rtc.h"
#include "spi.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "LoRa.h"
#include <stdbool.h>
#include <cJSON.h>
#include <stdlib.h>
#include <string.h>
#include <pwr.h>
#include <hx711.h>
#include <battery.h>

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
static char *SELF_SCALE_ID = "422fff08-4d49-49ab-8f7e-a431a595484a";
static bool SELF_SEND = false;
static const int LORA_BUFFER_SIZE = 100;

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

void init_weight(hx711_t *hx711){

	// Initialize the GPIO of the hx711 sensor.
	hx711_init(hx711, HX711_SCK_GPIO_Port, HX711_SCK_Pin, HX711_DT_GPIO_Port, HX711_DT_Pin);

	// Configure gain for each channel (see datasheet for details).
	set_gain(hx711, 128);

	// Scaling factor of the module is setting. This parameter need to testing for any scale.
	// The parameter need to set to one at the first time. The resulting value must then be divide by real value.
	// This will be the scale factor.
	set_scale(hx711, 29.69846392);

	// Offset value of the module is setting.
	// The value of parameter receive from the tare function. This parameter need to testing for any scale.
	set_offset(hx711, 382273);
}


// This function return the weight of the scale. If the result less then zero when return zero.
uint32_t measure_weight(hx711_t hx711){
	int32_t weight;
	weight = get_weight(&hx711, TEN_TIMES);
	weight = (weight < 0) ? 0 : weight;
	return weight;
}

// The time of the alarm need to setting up in this function.
void set_alarm_date_time(RTC_AlarmTypeDef *s_alarm) {
	RTC_TimeTypeDef s_time;
	HAL_RTC_GetTime(&hrtc, &s_time, RTC_FORMAT_BCD);
	if (s_time.Minutes >= 60) {
		s_time.Minutes -= 60;
		s_time.Hours += 1;
		if (s_time.Hours >= 24) {
			s_time.Hours = 0;
		}
	}
	s_alarm->AlarmTime = s_time;
	s_alarm->AlarmTime.Hours = 20;
	s_alarm->AlarmTime.Minutes = 55;
	s_alarm->Alarm = RTC_ALARM_A;
	s_alarm->AlarmDateWeekDaySel = RTC_ALARMDATEWEEKDAYSEL_DATE;
	s_alarm->AlarmMask = RTC_ALARMMASK_DATEWEEKDAY;
	s_alarm->AlarmSubSecondMask = RTC_ALARMSUBSECONDMASK_ALL;
	//This need to setup for 1. It's presenting the next day.
	s_alarm->AlarmDateWeekDay = 1;
}

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_RTC_Init();
  MX_SPI1_Init();
  MX_ADC1_Init();
  /* USER CODE BEGIN 2 */

	// Variables declarations.
	uint8_t lora_buffer[LORA_BUFFER_SIZE];
	uint8_t packet_size = 0;
	cJSON *measurement;
	cJSON *id;
	cJSON *mass;
	cJSON *battery;
	LoRa myLoRa;
	char* sending_data;
	RTC_AlarmTypeDef s_alarm;

	//Taking measurements
	uint32_t weight;
	uint8_t battery_in_p;
	hx711_t hx711;

	init_weight(&hx711);
	weight = measure_weight(hx711);
	battery_in_p = battery_in_percent(&hadc1);


	// Here need to wrap to JSON the self scale id, weight in gram and battery charge.
	measurement = cJSON_CreateObject();
	id = cJSON_CreateString(SELF_SCALE_ID);
	mass = cJSON_CreateNumber(weight);
	battery = cJSON_CreateNumber(battery_in_p);
	cJSON_AddItemToObject(measurement, "scale_id", id);
	cJSON_AddItemToObject(measurement, "mass", mass);
	cJSON_AddItemToObject(measurement, "battery", battery);
	sending_data = cJSON_PrintUnformatted(measurement);

	//LoRa module setups
	myLoRa = newLoRa();

	myLoRa.CS_port = NSS_GPIO_Port;
	myLoRa.CS_pin = NSS_Pin;
	myLoRa.reset_port = RESET_GPIO_Port;
	myLoRa.reset_pin = RESET_Pin;
	myLoRa.DIO0_port = DIO0_GPIO_Port;
	myLoRa.DIO0_pin = DIO0_Pin;
	myLoRa.hSPIx = &hspi1;

	myLoRa.frequency = 433;
	myLoRa.spredingFactor = SF_7;
	myLoRa.bandWidth = BW_125KHz;
	myLoRa.crcRate = CR_4_5;
	myLoRa.power = POWER_17db;
	myLoRa.overCurrentProtection = 130;
	myLoRa.preamble = 9;

	LoRa_init(&myLoRa);

	//Start receiving. It need the date at first time
	LoRa_startReceiving(&myLoRa);
	HAL_Delay(1000);

	while (packet_size <= 1) {
		packet_size = LoRa_receive(&myLoRa, (uint8_t*) lora_buffer, LORA_BUFFER_SIZE);
		HAL_Delay(5000);
	}

	//RTC date and time setup. I want to synchronize all time.
	set_rtc_date_time((char*)lora_buffer, &hrtc);
	//Delete buffer.
	packet_size = 0;
	memset((char*)lora_buffer, 0, (size_t)LORA_BUFFER_SIZE);
	HAL_Delay(100);

	//Waiting for data to be sent.
	//It must be sent when a request with your own ID arrives from the data collector
	while(!SELF_SEND)
	{
		while (packet_size <= 1) {
			packet_size = LoRa_receive(&myLoRa, (uint8_t *)lora_buffer, LORA_BUFFER_SIZE);
			HAL_Delay(5000);
			if(strcmp((char*)lora_buffer, (char*)SELF_SCALE_ID) == 0){
				SELF_SEND = true;
			}
		}
	}

	//The data can now be sent.
	memset((char*)lora_buffer, 0, (size_t) LORA_BUFFER_SIZE);

	strcpy((char*)lora_buffer, sending_data);
	HAL_Delay(1000);
	LoRa_transmit(&myLoRa, lora_buffer, LORA_BUFFER_SIZE, 10000);
	HAL_Delay(10000);

	//All dynamic variable free.
	free(sending_data);
	free(battery);
	free(mass);
	free(id);
	free(measurement);

	//Set alarm and the mcu, LoRa go to sleep.
	set_alarm_date_time(&s_alarm);
	LoRa_gotoMode(&myLoRa, SLEEP_MODE);
	go_to_sleep(&hrtc, s_alarm);


  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
	while (1) {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
}
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_LSI|RCC_OSCILLATORTYPE_MSI;
  RCC_OscInitStruct.LSIState = RCC_LSI_ON;
  RCC_OscInitStruct.MSIState = RCC_MSI_ON;
  RCC_OscInitStruct.MSICalibrationValue = 0;
  RCC_OscInitStruct.MSIClockRange = RCC_MSIRANGE_6;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_MSI;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
/* User can add his own implementation to report the HAL error return state */
__disable_irq();
while (1) {
}
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
