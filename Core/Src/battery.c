#include <battery.h>

uint16_t measure_battery(ADC_HandleTypeDef* adc)
{
	uint16_t adc_value = 0;
	HAL_ADC_Start(adc);
	if (HAL_ADC_PollForConversion(adc, 1000) == HAL_OK)
	{
		adc_value = HAL_ADC_GetValue(adc);
	}
	HAL_ADC_Stop(adc);
	return adc_value;
}

uint8_t battery_in_percent(ADC_HandleTypeDef* adc)
{
	uint8_t battery_in_percent = 0;
	float battery_voltage = 0;
	uint16_t adc_value = measure_battery(adc);
	battery_voltage = MAX_VOLTAGE * (float)((float)adc_value / (float)MAX_LSB);
	//It is scaled between 3.4V and 4.2V and convert percentage form.
	battery_in_percent = ((battery_voltage - MIN_VOLTAGE)) / (float)(MAX_VOLTAGE - MIN_VOLTAGE) * 100.0f;
	return battery_in_percent;
}
